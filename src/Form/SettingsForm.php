<?php

namespace Drupal\accessibe\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Datetime\DateFormatter;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class SettingsForm.
 *
 * @package Drupal\votingapi\Form
 *
 * @ingroup votingapi
 */
class SettingsForm extends ConfigFormBase {


  /**
   * Creates a CommentAdminOverview form.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->setConfigFactory($config_factory);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory')
    );
  }

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'accessibe_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['accessibe.settings'];
  }

  /**
   * Defines the settings form for Vote entities.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   Form definition array.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('accessibe.settings');

    $form['enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable AccessiBe'),
      '#default_value' => $config->get('enabled') ?? FALSE
    ];

    $form['show_admin'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show on administration page'),
      '#default_value' => $config->get('show_admin') ?? FALSE
    ];

    $form['custom'] = array(
      '#type' => 'fieldset',
      '#title' => t('customized Installation'),
      '#collapsible' => FALSE,
    );

    $form['custom']['statement_link'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Accessibility Statement Link'),
      '#default_value' => $config->get('statement_link') ?? '',
      '#size' => 40
    ];
    $form['custom']['feedback_link'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Feedback Form Link'),
      '#default_value' => $config->get('feedback_link') ?? '',
      '#size' => 40
    ];
    $form['custom']['footer_html'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Interface Footer'),
      '#default_value' => $config->get('footer_html') ?? '',
      '#size' => 40
    ];
    $form['custom']['lead_color'] = [
      '#type' => 'color',
      '#title' => $this->t('Interface Lead Color'),
      '#default_value' => $config->get('lead_color') ?? '#146ff8'
    ];
    $form['custom']['trigger_color'] = [
      '#type' => 'color',
      '#title' => $this->t('Trigger Button Color'),
      '#default_value' => $config->get('trigger_color') ?? '#146ff8'
    ];
    $form['custom']['position'] = [
      '#title' => $this->t('Interface Position'),
      '#type' => 'select',
      '#options' => ['left' => $this->t('Left'), 'right' => $this->t('Right')],
      '#required' => TRUE,
      //'#description' => t('The custom publishing option to use.'),
      '#default_value' => $config->get('position') ?? 'right'
    ];
    $form['custom']['hide_trigger'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Hide Trigger Button?'),
      '#default_value' => $config->get('hide_trigger') ?? FALSE
    ];
    $form['custom']['trigger_icon'] = [
      '#type' => 'radios',
      '#title' => $this->t('Trigger Icon'),
      '#default_value' => $config->get('trigger_icon') ?? 'default',
      '#options' => ['default' => $this->t('Default'), 'display' => $this->t('Display')],
    ];
    $form['custom']['trigger_position_x'] = [
      '#title' => $this->t('Trigger Horizontal Position'),
      '#type' => 'select',
      '#options' => ['left' => $this->t('Left'), 'right' => $this->t('Right')],
      //'#description' => t('The custom publishing option to use.'),
      '#default_value' => $config->get('trigger_position_x') ?? 'right',
      '#states' => [
        'invisible' => [
          ':input[name="hide_trigger"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['custom']['trigger_position_y'] = [
      '#title' => $this->t('Trigger Vertical Position'),
      '#type' => 'select',
      '#options' => [
        'top' => $this->t('Top'),
        'center' => $this->t('Center'),
        'bottom' => $this->t('Bottom')
      ],
      //'#description' => t('The custom publishing option to use.'),
      '#default_value' => $config->get('trigger_position_y') ?? 'bottom',
      '#states' => [
        'invisible' => [
          ':input[name="hide_trigger"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['custom']['trigger_size'] = [
      '#title' => $this->t('Trigger Button Size'),
      '#type' => 'select',
      '#options' => [
        'small' => $this->t('Small'),
        'medium' => $this->t('Medium'),
        'big' => $this->t('Big')
      ],
      //'#description' => t('The custom publishing option to use.'),
      '#default_value' => $config->get('trigger_size') ?? 'medium',
      '#states' => [
        'invisible' => [
          ':input[name="hide_trigger"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['custom']['trigger_radius'] = [
      '#title' => $this->t('Trigger Button Shape'),
      '#type' => 'select',
      '#options' => [
        '50%' => $this->t('Round'),
        '0' => $this->t('Square'),
        '10px' => $this->t('Squircle Big'),
        '5px' => $this->t('Squircle Small')
      ],
      //'#description' => t('The custom publishing option to use.'),
      '#default_value' => $config->get('trigger_radius') ?? 'medium',
      '#states' => [
        'invisible' => [
          ':input[name="hide_trigger"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['custom']['trigger_offset_x'] = [
      '#type' => 'number',
      '#title' => $this->t('Trigger Horizontal Offset'),
      '#default_value' => $config->get('trigger_offset_x') ?? 20,
      '#states' => [
        'invisible' => [
          ':input[name="hide_trigger"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['custom']['trigger_offset_y'] = [
      '#type' => 'number',
      '#title' => $this->t('Trigger Vertical Offset'),
      '#default_value' => $config->get('trigger_offset_y') ?? 20,
      '#states' => [
        'invisible' => [
          ':input[name="hide_trigger"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['mobile'] = array(
      '#type' => 'fieldset',
      '#title' => t('Mobile Configuration'),
      '#collapsible' => FALSE,
    );
    $form['mobile']['hide_mobile'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Hide On Mobile?'),
      '#default_value' => $config->get('hide_mobile') ?? FALSE
    ];
    $form['mobile']['mobile_trigger_position_x'] = [
      '#title' => $this->t('Trigger Horizontal Position'),
      '#type' => 'select',
      '#options' => ['left' => $this->t('Left'), 'right' => $this->t('Right')],
      //'#description' => t('The custom publishing option to use.'),
      '#default_value' => $config->get('mobile_trigger_position_x') ?? 'left',
      '#states' => [
        'invisible' => [
          ':input[name="hide_mobile"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['mobile']['mobile_trigger_position_y'] = [
      '#title' => $this->t('Trigger Vertical Position'),
      '#type' => 'select',
      '#options' => [
        'top' => $this->t('Top'),
        'center' => $this->t('Center'),
        'bottom' => $this->t('Bottom')
      ],
      //'#description' => t('The custom publishing option to use.'),
      '#default_value' => $config->get('mobile_trigger_position_y') ?? 'left',
      '#states' => [
        'invisible' => [
          ':input[name="hide_mobile"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['mobile']['mobile_trigger_size'] = [
      '#title' => $this->t('Trigger Button Size'),
      '#type' => 'select',
      '#options' => [
        'small' => $this->t('Small'),
        'medium' => $this->t('Medium'),
        'big' => $this->t('Big')
      ],
      //'#description' => t('The custom publishing option to use.'),
      '#default_value' => $config->get('mobile_trigger_size') ?? 'small',
      '#states' => [
        'invisible' => [
          ':input[name="hide_mobile"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['mobile']['mobile_trigger_radius'] = [
      '#title' => $this->t('Trigger Button Shape'),
      '#type' => 'select',
      '#options' => [
        '50%' => $this->t('Round'),
        '0' => $this->t('Square'),
        '10px' => $this->t('Squircle Big'),
        '5px' => $this->t('Squircle Small')
      ],
      //'#description' => t('The custom publishing option to use.'),
      '#default_value' => $config->get('mobile_trigger_radius') ?? 'medium',
      '#states' => [
        'invisible' => [
          ':input[name="hide_mobile"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['mobile']['mobile_trigger_offset_x'] = [
      '#type' => 'number',
      '#title' => $this->t('Trigger Horizontal Offset'),
      '#default_value' => $config->get('mobile_trigger_offset_x') ?? 0,
      '#states' => [
        'invisible' => [
          ':input[name="hide_mobile"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['mobile']['mobile_trigger_offset_y'] = [
      '#type' => 'number',
      '#title' => $this->t('Trigger Vertical Offset'),
      '#default_value' => $config->get('mobile_trigger_offset_y') ?? 0,
      '#states' => [
        'invisible' => [
          ':input[name="hide_mobile"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['#attached']['library'][] = 'accessibe/styles';
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('accessibe.settings');
    $settings = [
      'enabled',
      'show_admin',
      'statement_link',
      'feedback_link',
      'footer_html',
      'lead_color',
      'trigger_color',
      'position',
      'trigger_icon',
      'trigger_position_x',
      'trigger_position_y',
      'trigger_size',
      'trigger_radius',
      'hide_trigger',
      'trigger_offset_x',
      'trigger_offset_y',
      'hide_mobile',
      'mobile_trigger_position_x',
      'mobile_trigger_position_y',
      'mobile_trigger_size',
      'mobile_trigger_radius',
      'mobile_trigger_offset_x',
      'mobile_trigger_offset_y'
    ];
    foreach ($settings as $setting) {
      $config->set($setting, $form_state->getValue($setting));
    }
    $config->save();
    parent::submitForm($form, $form_state);
  }

}
