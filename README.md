AccessiBe API
------------------------

CONTENTS OF THIS FILE
---------------------

-   Introduction
-   Requirements
-   Installation
-   Configuration

INTRODUCTION
------------

This module aims to integrate AccessiBe API into Drupal.

See [accessibe.com](https://accessibe.com) for more details.

REQUIREMENTS
------------

Does not presently have any technical requirements.

INSTALLATION
------------

Installation is fairly standard, either by manually uploading the module
or using composer.

``` {.bash}
composer require drupal/accessibe
```

CONFIGURATION
-------------

Configuration can be found at `/admin/config/system/accessibe`.

